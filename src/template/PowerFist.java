package template;

public class PowerFist extends RocketGrab
{

	String[] abilityUsed = { "Slow", "Stun", "Root", "Cripple" };
	String[] passivesUsed = { "Lifesteal", "Steroid" };

	@Override
	boolean targetWantsOut()
	{
		return false;
	}

	@Override
	boolean targetUseHeal()
	{
		return false;
	}

	@Override
	public void useOverdrive()
	{

		System.out.print("Adding Exhaust: ");

		for (String fear : abilityUsed)
		{

			System.out.print(fear + " ");

		}

	}

	@Override
	public void useStaticField()
	{

		System.out.print("Adding Grab: ");

		for (String consumable : passivesUsed)
		{

			System.out.print(consumable + " ");

		}

	}

	@Override
	void useGrab()
	{
	}

	@Override
	void usePowerfist()
	{
	}

}

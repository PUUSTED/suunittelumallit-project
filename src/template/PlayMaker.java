package template;

public class PlayMaker
{

	public static void main(final String[] args)
	{

		BlitzThinkTank ritchTarget = new BlitzThinkTank();

		ritchTarget.makeAPlay();

		System.out.println();

		PowerFist poorTarget = new PowerFist();

		poorTarget.makeAPlay();

	}

}

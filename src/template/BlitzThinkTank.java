package template;

public class BlitzThinkTank extends RocketGrab
{

	String[] flashUsed = { "ADC", "Support", "Jungle" };
	String[] distanceUsed = { "Distance 5 feet" };
	String[] gankUsed = { "Jungle", "Missings", "Wards down", "Lane movement" };
	String[] wardUsed = { "Sight stone", "Trinkets" };

	@Override
	public void useGrab()
	{

		System.out.print("Checking summoner burns: ");

		for (String flash : flashUsed)
		{

			System.out.print(flash + " ");

		}

	}

	@Override
	public void usePowerfist()
	{

		System.out.print("Checking proximity: ");

		for (String lock : distanceUsed)
		{

			System.out.print(lock + " ");

		}

	}

	@Override
	public void useOverdrive()
	{

		System.out.print("Checking for counter gank: ");

		for (String gank : gankUsed)
		{

			System.out.print(gank + " ");

		}

	}

	@Override
	public void useStaticField()
	{

		System.out.print("Checking hostile range: ");

		for (String consumable : wardUsed)
		{

			System.out.print(consumable + " ");

		}

	}

}

package template;

public abstract class RocketGrab
{

	boolean afterFirstConsumable = false;

	// This is the Template Method
	// Declare this method final to keep subclasses from
	// changing the algorithm

	final void makeAPlay()
	{

		hitGrab();

		if (targetWantsOut())
		{

			useGrab();

			// Here to handle new lines for spacing
			afterFirstConsumable = true;

		}

		if (targetUseHeal())
		{

			if (afterFirstConsumable)
			{
				System.out.print("\n");
			}

			usePowerfist();

			afterFirstConsumable = true;

		}

		if (targetUseExhaust())
		{

			if (afterFirstConsumable)
			{
				System.out.print("\n");
			}

			useOverdrive();

			afterFirstConsumable = true;

		}

		if (targetUseAbilities())
		{

			if (afterFirstConsumable)
			{
				System.out.print("\n");
			}

			useStaticField();

			afterFirstConsumable = true;

		}

		lockDownTarget();

	}

	// These methods must be overridden by the extending subclasses

	abstract void useGrab();

	abstract void usePowerfist();

	abstract void useOverdrive();

	abstract void useStaticField();

	public void hitGrab()
	{

		System.out.println("The Target is hit");

	}

	// These are called hooks
	// If the user wants to override these they can

	// Use abstract methods when you want to force the user
	// to override and use a hook when you want it to be optional

	boolean targetWantsOut()
	{
		return true;
	}

	boolean targetUseHeal()
	{
		return true;
	}

	boolean targetUseExhaust()
	{
		return true;
	}

	boolean targetUseAbilities()
	{
		return true;
	}

	public void lockDownTarget()
	{

		System.out.println("\nLock down the Target");

	}

	public void afterFirstCondiment()
	{

		System.out.println("\n");

	}

}
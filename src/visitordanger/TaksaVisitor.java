package visitordanger;

import java.text.DecimalFormat;

class TaksaVisitor implements Visitor
{

	DecimalFormat df = new DecimalFormat("#.##");

	public TaksaVisitor()
	{
	}

	@Override
	public double kayti(final Liikoori likooriTavarat)
	{
		System.out.println("Likööri: Taksa hinta");
		return Double.parseDouble(df.format((likooriTavarat.getPrice() * .18) + likooriTavarat.getPrice()));
	}

	@Override
	public double kayti(final Suitsukkeet suitsukeTavarat)
	{
		System.out.println("Suitsuke: Taksa hinta");
		return Double.parseDouble(df.format((suitsukeTavarat.getPrice() * .32) + suitsukeTavarat.getPrice()));
	}

	@Override
	public double kayti(final Tarve tapeenTavarat)
	{
		System.out.println("Tarve: Taksa hinta");
		return Double.parseDouble(df.format(tapeenTavarat.getPrice()));
	}

}

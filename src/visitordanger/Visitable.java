package visitordanger;

interface Visitable
{

	public double accept(Visitor visitor);

}

package visitordanger;

class Tarve implements Visitable {
	
	private double hinta;

	Tarve(double item) {
		hinta = item;
	}

	public double accept(Visitor visitor) {
		return visitor.kayti(this);
	}

	public double getPrice() {
		return hinta;
	}
	
}
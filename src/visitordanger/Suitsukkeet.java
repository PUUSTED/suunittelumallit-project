package visitordanger;

class Suitsukkeet implements Visitable {
	
	private double hinta;

	Suitsukkeet(double item) {
		hinta = item;
	}

	public double accept(Visitor visitor) {
		return visitor.kayti(this);
	}

	public double getPrice() {
		return hinta;
	}
	
}
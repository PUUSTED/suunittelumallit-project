package visitordanger;

class Liikoori implements Visitable
{

	private double hinta;

	Liikoori(final double item)
	{
		hinta = item;
	}

	@Override
	public double accept(final Visitor visitor)
	{
		return visitor.kayti(this);
	}

	public double getPrice()
	{
		return hinta;
	}

}
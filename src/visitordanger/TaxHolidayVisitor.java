package visitordanger;

import java.text.DecimalFormat;

class TaxHolidayVisitor implements Visitor
{

	DecimalFormat df = new DecimalFormat("#.##");

	public TaxHolidayVisitor()
	{
	}

	@Override
	public double kayti(final Liikoori liquorItem)
	{
		System.out.println("Likööri: Taksa hinta");
		return Double.parseDouble(df.format((liquorItem.getPrice() * .10) + liquorItem.getPrice()));
	}

	@Override
	public double kayti(final Suitsukkeet tobaccoItem)
	{
		System.out.println("Suitsuke: Taksa hinta");
		return Double.parseDouble(df.format((tobaccoItem.getPrice() * .30) + tobaccoItem.getPrice()));
	}

	@Override
	public double kayti(final Tarve necessityItem)
	{
		System.out.println("Tarve: Taksa hinta");
		return Double.parseDouble(df.format(necessityItem.getPrice()));
	}

}

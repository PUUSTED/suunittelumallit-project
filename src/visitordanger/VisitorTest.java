package visitordanger;

public class VisitorTest
{
	public static void main(final String[] args)
	{

		TaksaVisitor taxLas = new TaksaVisitor();
		TaxHolidayVisitor taxLomaLas = new TaxHolidayVisitor();

		Tarve maito = new Tarve(3.47);
		Liikoori vodka = new Liikoori(11.99);
		Suitsukkeet sikaari = new Suitsukkeet(19.99);

		System.out.println(maito.accept(taxLas) + "\n");
		System.out.println(vodka.accept(taxLas) + "\n");
		System.out.println(sikaari.accept(taxLas) + "\n");

		System.out.println("TAKSA HINNOITTELUT\n");

		System.out.println(maito.accept(taxLomaLas) + "\n");
		System.out.println(vodka.accept(taxLomaLas) + "\n");
		System.out.println(sikaari.accept(taxLomaLas) + "\n");

	}
}
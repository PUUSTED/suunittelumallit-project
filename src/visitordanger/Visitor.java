package visitordanger;

interface Visitor
{

	public double kayti(Liikoori likooriTavarat);

	public double kayti(Suitsukkeet suitsukeTavarat);

	public double kayti(Tarve tapeenTavarat);

}
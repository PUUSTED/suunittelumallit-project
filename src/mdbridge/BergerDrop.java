package mdbridge;

public class BergerDrop extends BergerKing
{

	public BergerDrop(final McDonalds newBerger)
	{
		super(newBerger);
	}

	@Override
	public void prestoBerger()
	{

		System.out.println("Berger was tossed");

	}

}
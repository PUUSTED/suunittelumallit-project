package mdbridge;

abstract class McDonalds
{

	public int bergerState;

	public int maxMenu;

	public int volumeTaso = 0;

	public abstract void bunAdded();

	public abstract void beefAdded();

	public void meyoAdded()
	{

		if (bergerState > maxMenu || bergerState < 0)
		{
			bergerState = 0;
		}

		System.out.println("On Channel " + bergerState);

	}

	public void buttonSevenPressed()
	{

		volumeTaso++;

		System.out.println("Volume at: " + volumeTaso);

	}

	public void buttonEightPressed()
	{

		volumeTaso--;

		System.out.println("Volume at: " + volumeTaso);

	}

}
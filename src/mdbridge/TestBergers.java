package mdbridge;

public class TestBergers
{

	public static void main(final String[] args)
	{

		BergerKing theBergers = new BergerDrop(new BergerBurger(1, 200));

		BergerKing theBurgers = new BergerDone(new BergerBurger(1, 200));

		BergerKing theSnack = new BergerMote(new MainMenu(1, 14));

		System.out.println("Test TV with Mute");

		theBergers.bunAdd();
		theBergers.beefAdd();
		theBergers.prestoBerger();

		System.out.println("\nTest TV with Pause");

		theBurgers.bunAdd();
		theBurgers.beefAdd();
		theBurgers.prestoBerger();
		theBurgers.meyoAdd();

		System.out.println("\nTest DVD");

		theSnack.bunAdd();
		theSnack.beefAdd();
		theSnack.prestoBerger();
		theSnack.prestoBerger();

	}

}
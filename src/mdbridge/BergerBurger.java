package mdbridge;

public class BergerBurger extends McDonalds
{

	public BergerBurger(final int newBergerState, final int newMaxMenu)
	{

		bergerState = newBergerState;

		maxMenu = newMaxMenu;

	}

	@Override
	public void bunAdded()
	{

		System.out.println("Berger flop");

		bergerState--;

	}

	@Override
	public void beefAdded()
	{

		System.out.println("Berger flip");

		bergerState++;

	}

}
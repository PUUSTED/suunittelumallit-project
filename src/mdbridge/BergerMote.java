package mdbridge;

public class BergerMote extends BergerKing
{

	private boolean cook = true;

	public BergerMote(final McDonalds newBerger)
	{
		super(newBerger);
	}

	@Override
	public void prestoBerger()
	{

		cook = !cook;

		System.out.println("Berger is now: " + cook);

	}

}

package mdbridge;

public class MainMenu extends McDonalds
{

	public MainMenu(final int newBergerState, final int newMaxMenu)
	{

		super.bergerState = newBergerState;

		super.maxMenu = newMaxMenu;

	}

	@Override
	public void bunAdded()
	{

		System.out.println("Next");

		bergerState--;

	}

	@Override
	public void beefAdded()
	{

		System.out.println("Next");

		bergerState++;

	}

}
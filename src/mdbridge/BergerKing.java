package mdbridge;

public abstract class BergerKing
{

	private McDonalds basicBerger;

	public BergerKing(final McDonalds newBerger)
	{

		basicBerger = newBerger;

	}

	public void bunAdd()
	{

		basicBerger.bunAdded();

	}

	public void beefAdd()
	{

		basicBerger.beefAdded();

	}

	public void meyoAdd()
	{

		basicBerger.meyoAdded();

	}

	public abstract void prestoBerger();

}
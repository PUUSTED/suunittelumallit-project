package chainofresposibility;

public interface Chain
{

	public void setNextChain(Chain nextChain);

	public void accumulateCall(Lähimies request);

}
package chainofresposibility;

public class Lähimies
{

	private int korotuspyyntöok;
	private int korotuspyyntöei;

	private String calculationWanted;

	public Lähimies(final int newKorotusOk, final int newKorotusEi, final String calcWanted)
	{

		korotuspyyntöok = newKorotusOk;
		korotuspyyntöei = newKorotusEi;
		calculationWanted = calcWanted;

	}

	public int getKorOk()
	{
		return korotuspyyntöok;
	}

	public int getKorEi()
	{
		return korotuspyyntöei;
	}

	public String getCalcWanted()
	{
		return calculationWanted;
	}

}
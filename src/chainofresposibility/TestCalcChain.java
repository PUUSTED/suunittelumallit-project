package chainofresposibility;

public class TestCalcChain
{

	public static void main(final String[] args)
	{

		Chain chainCalc1 = new AddKorko();

		Lähimies request = new Lähimies(2, 5, "OK");

		chainCalc1.accumulateCall(request);

	}

}
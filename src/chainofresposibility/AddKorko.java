package chainofresposibility;

public class AddKorko implements Chain
{

	private Chain nextInChain;

	// Defines the next Object to receive the
	// data if this one can't use it

	@Override
	public void setNextChain(final Chain nextChain)
	{

		nextInChain = nextChain;

	}

	// Tries to calculate the data, or passes it
	// to the Object defined in method setNextChain()

	@Override
	public void accumulateCall(final Lähimies request)
	{

		if (request.getCalcWanted() == "OK")
		{

			System.out.print(request.getKorOk() + " " + "Palkankorotuspyyntö hyväksytty.");

		}
		else
		{

			System.out.println(request.getKorEi() + "Palkankorotuspyyntö hylätty.");

		}

	}
}
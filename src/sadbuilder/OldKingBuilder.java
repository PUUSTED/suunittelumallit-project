package sadbuilder;

public class OldKingBuilder implements HerbertKing
{

	private Burger burger;

	public OldKingBuilder()
	{

		this.burger = new Burger();

	}

	@Override
	public void buildBurgerHead()
	{

		burger.builderBurgersHead("Bun Head");

	}

	@Override
	public void buildBurgerMeat()
	{

		burger.builderBurgersMeat("Meat");

	}

	@Override
	public void buildBurgerSauce()
	{

		burger.builderBurgersMeyo("Ketsup");

	}

	@Override
	public void buildBurgerBottom()
	{

		burger.builderBurgersBottom("Bun Bot");

	}

	@Override
	public Burger getBurger()
	{

		return this.burger;
	}

}
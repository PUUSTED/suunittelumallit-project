package sadbuilder;

public interface McHerbert
{

	public void builderBurgersHead(String top);

	public void builderBurgersMeat(String mid);

	public void builderBurgersMeyo(String add);

	public void builderBurgersBottom(String bot);

}

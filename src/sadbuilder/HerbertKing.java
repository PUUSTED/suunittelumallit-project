package sadbuilder;

public interface HerbertKing {
	
	public void buildBurgerHead();
	
	public void buildBurgerMeat();
	
	public void buildBurgerSauce();
	
	public void buildBurgerBottom();
	
	public Burger getBurger();
	
}
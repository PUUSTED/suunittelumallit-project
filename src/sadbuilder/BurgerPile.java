package sadbuilder;

public class BurgerPile
{

	private HerbertKing herbertKing;

	public BurgerPile(final HerbertKing herbertKing)
	{

		this.herbertKing = herbertKing;

	}

	public Burger getBurger()
	{

		return this.herbertKing.getBurger();

	}

	public void makeBurger()
	{

		this.herbertKing.buildBurgerHead();
		this.herbertKing.buildBurgerMeat();
		this.herbertKing.buildBurgerSauce();
		this.herbertKing.buildBurgerBottom();

	}

}
package sadbuilder;

public class TestBurgers
{

	public static void main(final String[] args)
	{

		HerbertKing oldStyleRobot = new OldKingBuilder();

		BurgerPile burgerPile = new BurgerPile(oldStyleRobot);

		burgerPile.makeBurger();

		Burger firstRobot = burgerPile.getBurger();

		System.out.println("Burger Built");

		System.out.println("Burger top Type: " + firstRobot.getBurgertHead());

		System.out.println("Burger meat Type: " + firstRobot.getBurgerMeat());

		System.out.println("Burger meyo Type: " + firstRobot.getBurgerMeyo());

		System.out.println("Burger bottom Type: " + firstRobot.getBurgerBottom());

	}

}
package sadbuilder;

public class Burger implements McHerbert{

	private String burgerHead;
	private String burgerMeat;
	private String burgerMeyo;
	private String burgerBottom;
	
	public void builderBurgersHead(String top) {
		
		burgerHead = top;
		
	}
	
	public String getBurgertHead(){ return burgerHead; }

	
	public void builderBurgersMeat(String mid) {
		
		burgerMeat = mid;
		
	}
	
	public String getBurgerMeat(){ return burgerMeat; }

	
	public void builderBurgersMeyo(String add) {
		
		burgerMeyo = add;
		
	}
	
	public String getBurgerMeyo(){ return burgerMeyo; }

	
	public void builderBurgersBottom(String bot) {
		
		burgerBottom = bot;
		
	}
	
	public String getBurgerBottom(){ return burgerBottom; }
	
	
	
}
package proxy;

public interface AnnaData
{
	public KuvaStat getKuvaStat();

	public int getKuvaInKone();
}

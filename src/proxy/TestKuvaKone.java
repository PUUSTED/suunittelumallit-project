package proxy;

public class TestKuvaKone
{

	public static void main(final String[] args)

	{
		AnnaData realKuvaKone = new KuvaKone();

		AnnaData kuvaProxy = new KuvaProxy();

		System.out.println("\nCurrent Kuva State " + kuvaProxy.getKuvaInKone());

		System.out.println("\nData in Kuva Kone $" + kuvaProxy.getKuvaInKone());
	}

}

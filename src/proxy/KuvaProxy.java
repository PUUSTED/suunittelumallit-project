package proxy;

public class KuvaProxy implements AnnaData
{

	@Override
	public KuvaStat getKuvaStat()
	{

		KuvaKone realKuvaKone = new KuvaKone();

		return realKuvaKone.getKuvaStat();
	}

	@Override
	public int getKuvaInKone()
	{

		KuvaKone realKuvaKone = new KuvaKone();

		return realKuvaKone.getKuvaInKone();
	}

}
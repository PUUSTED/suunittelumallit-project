package proxy;

public interface KuvaStat
{

	void insertKuva();

	void ejectKuva();

	void insertData(int dataEntered);

	void requestData(int dataToWithdraw);

}

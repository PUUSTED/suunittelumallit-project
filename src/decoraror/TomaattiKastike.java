package decoraror;

public class TomaattiKastike extends PohjaPuskuri
{

	public TomaattiKastike(final Pizza uusPizza)
	{
		super(uusPizza);

		System.out.println("Kastikkeen Lisäys");
	}

	// Returns the result of calling getDescription() for
	// PlainPizza, Mozzarella and then TomatoSauce

	@Override
	public String getStuff()
	{

		return tempPizza.getStuff() + ", tomaatti kastike";

	}

	@Override
	public double getHinta()
	{

		System.out.println("Kastike hinta: " + .66);

		return tempPizza.getHinta() + .66;

	}

}
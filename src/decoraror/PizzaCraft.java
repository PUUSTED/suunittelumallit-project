package decoraror;

public class PizzaCraft
{

	public static void main(final String[] args)
	{

		// The PlainPizza object is sent to the Mozzarella constructor
		// and then to the TomatoSauce constructor

		Pizza normiPizza = new TomaattiKastike(new Mozzarella(new NormiPizza()));

		System.out.println("Ainekset: " + normiPizza.getStuff());

		System.out.println("Loppusumma: " + normiPizza.getHinta());

	}

}

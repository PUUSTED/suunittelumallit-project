package decoraror;

abstract class PohjaPuskuri implements Pizza
{

	protected Pizza tempPizza;

	// Assigns the type instance to this attribute
	// of a Pizza

	// All decorators can dynamically customize the Pizza
	// instance PlainPizza because of this

	public PohjaPuskuri(final Pizza newPizza)
	{

		tempPizza = newPizza;

	}

	@Override
	public String getStuff()
	{

		return tempPizza.getStuff();

	}

	@Override
	public double getHinta()
	{

		return tempPizza.getHinta();

	}

}
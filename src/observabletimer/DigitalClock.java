package observabletimer;

public class DigitalClock
{
	private ClockTimer timer;

	public DigitalClock(final ClockTimer timer2, final ClockTimer ct)
	{
		this.update(timer2);
		timer = ct;
		timer.attach(this);
	}

	public void update(final Subject s)
	{

		if (s == timer)
			draw();
	}

	private void draw()
	{
		int hour = timer.getHour();
		int minute = timer.getMinute();
		int second = timer.getSecond();

		System.out.println(hour + " " + minute + " " + second + " ");
	}
}

package observabletimer;

import java.util.Observable;
import java.util.Observer;

public class SudiApp
{
	ClockTimer timer = new ClockTimer();

	AnalogClock analogClock = new AnalogClock(timer);
	DigitalClock digitalClock = new DigitalClock(timer, timer);

	public static void main(final String[] args)
	{
		System.out.println("Set Time > ");

		EventSource eventSource = new EventSource();

		eventSource.addObserver(new Observer()
		{

			@Override
			public void update(final Observable o, final Object arg)
			{
				System.out.println("\nReceived lasagne: " + arg);
			}
		});
		new Thread(eventSource).start();
	}
}

package observabletimer;

public interface Observer
{
	public void update(double AnalogClock, double DigitalClock, double ClockTimer);
}

/*
 * public class Subject
 * {
 * Set<Observer> observers = new HashSet();
 * 
 * public void attach(final Observer o)
 * {
 * observers.add(o);
 * }
 * 
 * public void detach(final Observer o)
 * {
 * observers.remove(o);
 * }
 * 
 * @Override
 * protected void notify()
 * {
 * for (Observer o : observers)
 * {
 * update(this);
 * }
 * }
 * }
 */

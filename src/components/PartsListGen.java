package components;

public class PartsListGen
{

	public static void main(final String[] args)
	{

		ComponentKotelo muistipiiri = new PartsGroup("Muistipiiri", " ");

		ComponentKotelo emolevy = new PartsGroup("\nEmolevy", " ");

		ComponentKotelo prosessori = new PartsGroup("\nProsessori", " ");

		ComponentKotelo verkkokortti = new PartsGroup("\nVerkkokortti", " ");

		ComponentKotelo näytönohjain = new PartsGroup("\nProsessori", " ");

		ComponentKotelo kotelo = new PartsGroup("\nKotelo", " ");

		ComponentKotelo everyKotelo = new PartsGroup("Parts List", "Every Part Available");

		everyKotelo.add(muistipiiri);

		muistipiiri.add(new Kotelo("Musta Kotelo", "muistipiiri", 118));

		everyKotelo.add(emolevy);

		emolevy.add(new Kotelo("Musta Kotelo", "emolevy", 220));

		everyKotelo.add(prosessori);

		emolevy.add(new Kotelo("Musta Kotelo", "prosessori", 300));

		everyKotelo.add(verkkokortti);

		emolevy.add(new Kotelo("Musta Kotelo", "verkkokortti", 90));

		everyKotelo.add(näytönohjain);

		emolevy.add(new Kotelo("Musta Kotelo", "näytönohjain", 260));

		everyKotelo.add(kotelo);

		emolevy.add(new Kotelo("Musta Kotelo", "kotelo", 80));

		Core mustaKotelo = new Core(everyKotelo);

		mustaKotelo.getKoteloList();

	}

}

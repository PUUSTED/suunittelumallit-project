package components;

public abstract class ComponentKotelo
{
	public void add(final ComponentKotelo newComponentKotelo)
	{
		throw new UnsupportedOperationException();
	}

	public void remove(final ComponentKotelo newComponentKotelo)
	{
		throw new UnsupportedOperationException();
	}

	public ComponentKotelo getComponent(final int componentIndex)
	{
		throw new UnsupportedOperationException();
	}

	public String getKoteloName()
	{
		throw new UnsupportedOperationException();
	}

	public String getPartsName()
	{
		throw new UnsupportedOperationException();
	}

	public int getAccCostYear()
	{
		throw new UnsupportedOperationException();
	}

	public void displayKoteloInfo()
	{
		throw new UnsupportedOperationException();
	}

}

package components;

import java.util.ArrayList;
import java.util.Iterator;

public class PartsGroup extends ComponentKotelo
{

	// Contains any Songs or SongGroups that are added
	// to this ArrayList

	ArrayList componentsKotelo = new ArrayList();

	String groupName;
	String groupDescription;

	public PartsGroup(final String newGroupName, final String newGroupDescription)
	{

		groupName = newGroupName;
		groupDescription = newGroupDescription;

	}

	public String getGroupName()
	{
		return groupName;
	}

	public String getGroupDescription()
	{
		return groupDescription;
	}

	@Override
	public void add(final ComponentKotelo newComponentKotelo)
	{

		componentsKotelo.add(newComponentKotelo);

	}

	@Override
	public void remove(final ComponentKotelo newComponentKotelo)
	{

		componentsKotelo.remove(newComponentKotelo);

	}

	@Override
	public ComponentKotelo getComponent(final int componentIndex)
	{

		return (ComponentKotelo) componentsKotelo.get(componentIndex);

	}

	@Override
	public void displayKoteloInfo()
	{

		System.out.println(getGroupName() + " " + getGroupDescription() + "\n");

		Iterator koteloIterator = componentsKotelo.iterator();

		while (koteloIterator.hasNext())
		{

			ComponentKotelo koteloInfo = (ComponentKotelo) koteloIterator.next();

			koteloInfo.displayKoteloInfo();

		}

	}

}
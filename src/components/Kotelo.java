package components;

public class Kotelo extends ComponentKotelo
{

	String koteloName;
	String partsName;
	int accCost;

	public Kotelo(final String newKoteloName, final String newPartsName, final int newAccCost)
	{

		koteloName = newKoteloName;
		partsName = newPartsName;
		accCost = newAccCost;

	}

	@Override
	public String getKoteloName()
	{
		return koteloName;
	}

	@Override
	public String getPartsName()
	{
		return partsName;
	}

	public int getAccCost()
	{
		return accCost;
	}

	@Override
	public void displayKoteloInfo()
	{

		System.out.println(" This koppa is " + getKoteloName() + " and has part " + getPartsName() + " value " + getAccCost());

	}

}

package state;

public class Kadabra extends PokeState
{

	private Kadabra()
	{
	};

	private static Kadabra INSTANCE = null;

	public static Kadabra getInstance()
	{
		if (INSTANCE == null)
		{
			INSTANCE = new Kadabra();
		}
		return INSTANCE;
	}

	@Override
	void gainUp()
	{
		System.out.println("Kadabra uses Inner focus");
	}

	@Override
	void gain()
	{
		System.out.println("After focusing, Kadabra uses synchronize");

	}

	@Override
	void growth(final int storeGain, final Pokemon thing)
	{
		System.out.println("Kadabra grows " + storeGain);
		if (storeGain > 18)
		{
			thing.changeState(Alakazam.getInstance());
		}
	}

}

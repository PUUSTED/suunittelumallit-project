package state;

public class Alakazam extends PokeState
{

	private Alakazam()
	{
	};

	private static Alakazam INSTANCE = null;

	public static Alakazam getInstance()
	{
		if (INSTANCE == null)
		{
			INSTANCE = new Alakazam();
		}
		return INSTANCE;
	}

	@Override
	void gainUp()
	{
		System.out.println("Alakazam uses Synchronize and Inner Focus at the same time");
	}

	@Override
	void gain()
	{
		System.out.println("Activating Hidden ability");
	}

	@Override
	void growth(final int storeGain, final Pokemon thing)
	{
		System.out.println("Alakazam grows " + storeGain);

	}

}

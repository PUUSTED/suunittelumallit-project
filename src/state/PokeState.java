package state;

public abstract class PokeState
{
	abstract void gainUp();

	abstract void gain();

	abstract void growth(int gainedGrowth, Pokemon thing);

	void changeState(final Pokemon thing, final PokeState state)
	{
		thing.changeState(state);
	}
}

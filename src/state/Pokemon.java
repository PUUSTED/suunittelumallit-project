package state;

public class Pokemon
{
	private PokeState state;

	private int storeGain;

	public Pokemon()
	{
		state = Abra.getInstance();
	}

	void growth(final int gainedGrowth)
	{
		storeGain += gainedGrowth;
		state.growth(storeGain, this);
	}

	public void gainUp()
	{
		state.gainUp();
	}

	public void gain()
	{
		state.gain();
	}

	public void changeState(final PokeState state)
	{
		this.state = state;
	}

}

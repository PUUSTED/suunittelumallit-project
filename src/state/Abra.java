package state;

public class Abra extends PokeState
{

	private Abra()
	{
	};

	private static Abra INSTANCE = null;

	public static Abra getInstance()
	{
		if (INSTANCE == null)
		{
			INSTANCE = new Abra();
		}
		return INSTANCE;
	}

	@Override
	void gainUp()
	{

		System.out.println("Abra uses Inner focus");
	}

	@Override
	void gain()
	{
		System.out.println("After focusing, Abra Synchronizes with user!");

	}

	@Override
	void growth(final int storeGain, final Pokemon thing)
	{
		System.out.println("Abra grows " + storeGain);
		if (storeGain > 8)
		{
			thing.changeState(Kadabra.getInstance());
		}
	}

}

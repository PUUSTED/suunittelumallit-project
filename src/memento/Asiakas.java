package memento;

public class Asiakas
{

	private String luku;

	// Sets the value for the article

	public void set(final String newLuku)
	{
		System.out.println("Asikas: Hae uusi luku\n" + newLuku + "\n");
		this.luku = newLuku;
	}

	public Arvuuttaja storeLuku()
	{
		System.out.println("Asiakas: Save Luku");
		return new Arvuuttaja(luku);
	}

	public String restoreFromMemento(final Arvuuttaja arvuuttaja)
	{

		luku = arvuuttaja.getLuku();

		System.out.println("Asiakas: Edellinen Luku Saved in Arvuuttaja\n" + luku + "\n");

		return luku;

	}

}

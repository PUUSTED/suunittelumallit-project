package memento;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class TestArvuuttaja extends JFrame
{
	public static void main(final String[] args)
	{

		TestArvuuttaja testArvuuttaja = new TestArvuuttaja();

	}

	private JButton saveBut, undoBut, redoBut;

	private JTextArea theNumero = new JTextArea(40, 60);

	Luku luku = new Luku();

	Asiakas asiakas = new Asiakas();

	int saveLmk = 0, currentLkm = 0;

	public TestArvuuttaja()
	{

		this.setSize(300, 300);
		this.setTitle("Memento");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel1 = new JPanel();

		panel1.add(new JLabel("Numero"));

		panel1.add(theNumero);

		ButtonListener saveListener = new ButtonListener();
		ButtonListener undoListener = new ButtonListener();
		ButtonListener redoListener = new ButtonListener();

		saveBut = new JButton("Save");
		saveBut.addActionListener(saveListener);

		undoBut = new JButton("Undo");
		undoBut.addActionListener(undoListener);

		redoBut = new JButton("Redo");
		redoBut.addActionListener(redoListener);

		panel1.add(saveBut);
		panel1.add(undoBut);
		panel1.add(redoBut);

		this.add(panel1);

		this.setVisible(true);

	}

	class ButtonListener implements ActionListener
	{

		@Override
		public void actionPerformed(final ActionEvent e)
		{

			if (e.getSource() == saveBut)
			{

				String textInTextArea = theNumero.getText();

				asiakas.set(textInTextArea);

				luku.addArvaus(asiakas.storeLuku());

				saveLmk++;
				currentLkm++;

				System.out.println("Save Files " + saveLmk);

				undoBut.setEnabled(true);

			}
			else

			if (e.getSource() == undoBut)
			{

				if (currentLkm >= 1)
				{

					currentLkm--;

					String textBoxString = asiakas.restoreFromMemento(luku.getArvaus(currentLkm));

					theNumero.setText(textBoxString);

					redoBut.setEnabled(true);

				}
				else
				{

					undoBut.setEnabled(false);

				}

			}
			else

			if (e.getSource() == redoBut)
			{

				if ((saveLmk - 1) > currentLkm)
				{

					currentLkm++;

					String textBoxString = asiakas.restoreFromMemento(luku.getArvaus(currentLkm));

					theNumero.setText(textBoxString);

					undoBut.setEnabled(true);

				}
				else
				{

					redoBut.setEnabled(false);

				}

			}

		}

	}

}

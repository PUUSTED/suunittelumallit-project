package memento;

import java.util.ArrayList;

class Luku
{

	ArrayList<Arvuuttaja> savedLuvut = new ArrayList<Arvuuttaja>();

	public void addArvaus(final Arvuuttaja m)
	{
		savedLuvut.add(m);
	}

	public Arvuuttaja getArvaus(final int index)
	{
		return savedLuvut.get(index);
	}
}
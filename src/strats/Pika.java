package strats;

import java.util.Random;

public class Pika
{
	final static int MAX = 200000;
	static long laskuri = 0;

	public static void quickSortLoop()
	{
		int taulukko[] = new int[MAX];
		Random r = new Random();
		laskuri++;

		for (int i = 0; i < MAX; i++)
		{
			taulukko[i] = r.nextInt(1000);
		}
		System.out.println("\nSuoritetaan Quicksorttia, paina Enter");
		Lue.merkki();
		qs(taulukko, MAX);
		System.out.println("Valmis");
		laskuri++;
	}

	public static void quickSort(final int table[], final int lo0, final int hi0)
	{
		int lo = lo0;
		int hi = hi0;
		int mid, swap;

		// System.out.println(table.length+" "+lo0 + " " + hi0);
		mid = table[(lo0 + hi0) / 2];
		laskuri++;
		while (lo <= hi)
		{
			laskuri++;
			while (table[lo] < mid)
			{
				++lo;
				laskuri++;
			}
			laskuri++;
			while (table[hi] > mid)
			{
				--hi;
				laskuri++;
			}
			laskuri++;
			if (lo <= hi)
			{
				swap = table[lo];
				table[lo] = table[hi];
				++lo;
				table[hi] = swap;
				--hi;
			}
			laskuri++;
		}
		laskuri++;
		if (lo0 < hi)
		{
			quickSort(table, lo0, hi);
		}
		laskuri++;
		if (lo < hi0)
		{
			quickSort(table, lo, hi0);
		}
		laskuri++;
	}

	public static void qs(final int table[], final int values)
	{
		quickSort(table, 0, values - 1);

		System.out.println("\nJärjestetty aineisto:\n");
		laskuri++;
		System.out.println("\nKuittaa tulos, paina Enter " + laskuri);
		Lue.merkki();
	}
}

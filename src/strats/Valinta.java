package strats;

import java.util.Random;

public class Valinta
{
	final static int MAX = 200000;
	static long laskuri = 0;

	public static void selectSort()
	{

		int[] taul = new int[MAX];
		int i, j, k, apu, pienin;
		Random r = new Random();
		System.out.println("Generoidaan syöttöaineisto: ");
		for (i = 0; i < MAX; i++)
		{

			taul[i] = r.nextInt(1000);
		}
		System.out.println("\nSuoritetaan valintalajittelu, paina Enter ");
		Lue.merkki();

		laskuri++;
		for (i = 0; i < MAX; i++)
		{
			pienin = i;
			laskuri++;
			for (j = i + 1; j < MAX; j++)
			{
				laskuri++;
				if (taul[j] < taul[pienin])
				{
					pienin = j;
				}
				laskuri++;
			}
			laskuri++;
			if (pienin != i)
			{
				apu = taul[pienin];
				taul[pienin] = taul[i];
				taul[i] = apu;
			}
			laskuri++;
		}
		System.out.println();
		for (i = 0; i < MAX; i++)
		{
		}
		System.out.println("\nKuittaa tulos, paina Enter " + laskuri);
		Lue.merkki();

	}
}

package strats;

public class StratsSorting
{

	public static void main(final String[] args)
	{

		Valinta.selectSort();
		Lomitus.mergeSort();
		Pika.quickSortLoop();
	}

}

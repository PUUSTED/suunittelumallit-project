package strats;

import java.util.Random;

public class Lomitus
{
	final static int MAX = 200000;
	static long laskuri = 0;

	private static int[] tau = new int[MAX]; // aputaulukko (ei varata tätä pinosta!)

	public static void mergeSort()
	{
		int[] a = new int[MAX];
		int i;

		Random r = new Random(); // luodaan satunnaislukugeneraattori
		System.out.println("\nGeneroidaan syöttöaineisto: ");
		for (i = 0; i < MAX; i++)
		{
			a[i] = r.nextInt(1000); // generoidaan luvut
			// System.out.print(a[i]+" ");
			// if (i>0 && i%40==0) // rivinvaihto
			// System.out.println();
		}
		System.out.println("\nSuoritetaan lomituslajittelu, paina Enter ");
		Lue.merkki();

		mergeSort(a, 0, MAX - 1);
		// for (i=0;i<MAX;i++) {
		// System.out.print(a[i]+" ");
		// if (i>0 && i%40==0) // rivinvaihto
		// System.out.println();
		// }
		System.out.println("\nKuittaa tulos, paina Enter " + laskuri);
	}

	// oletus: osataulukot t[p..q] ja t[q+1...r] ovat järjestyksess„
	public static void merge(final int t[], final int p, final int q, final int r)
	{
		// i osoittaa 1. osataulukkoa, j osoittaa 2. osataulukkoa
		// k osoittaa aputaulukkoa, johon yhdiste kirjoitetaan.

		int i = p, j = q + 1, k = 0;
		laskuri += 2;
		while (i < q + 1 && j < r + 1)
		{
			laskuri++;
			if (t[i] < t[j])
			{
				tau[k++] = t[i++];
			}
			else
			{
				tau[k++] = t[j++];
			}
			laskuri += 2;
		}

		// toinen osataulukko käsitelty, siirretään toisen käsittelemättömät
		laskuri++;
		while (i < q + 1)
		{
			tau[k++] = t[i++];
			laskuri++;
		}
		laskuri++;
		while (j < r + 1)
		{
			tau[k++] = t[j++];
			laskuri++;
		}

		// siirretään yhdiste alkuperäiseen taulukkoon
		laskuri++;
		for (i = 0; i < k; i++)
		{
			t[p + i] = tau[i];
			laskuri++;
		}
	}

	public static void mergeSort(final int t[], final int alku, final int loppu)
	{
		int ositus;
		long la, ll, lt;
		laskuri++;
		if (alku < loppu)
		{

			la = alku;
			ll = loppu;
			lt = (la + ll) / 2;
			ositus = (int) lt;
			mergeSort(t, alku, ositus);
			mergeSort(t, ositus + 1, loppu);
			merge(t, alku, ositus, loppu);
		}
	}
}

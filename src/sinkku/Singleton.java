package sinkku;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Singleton
{

	String[] shuffleStuff = { "Opettaja", "Opiskelia", "Jätkä", "Pätkä", "Pentu", "Pullo", "Kurko", "Ässä", "Akka", "Ankka", };

	private LinkedList<String> stuffList = new LinkedList<String>(Arrays.asList(shuffleStuff));

	private List<?> letterList;

	static boolean THREAD = true;

	private Singleton()
	{
	};
	// huom! privatekonstruktori,
	// ei voida luoda luokan ulkopuolelta!

	private static Singleton INSTANCE = null;

	public static Singleton getInstance()
	{
		if (INSTANCE == null)
		{
			if (THREAD)
			{
				THREAD = false;

				Thread.currentThread();
				try
				{
					Thread.sleep(1000);
				}
				catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				synchronized (Singleton.class)
				{

					if (INSTANCE == null)
					{
						// If the instance isn't needed it isn't created
						// This is known as lazy instantiation

						INSTANCE = new Singleton();

						// Shuffle the letters in the list
						Collections.shuffle(INSTANCE.letterList);

					}

				}

			}
		}
		return INSTANCE = new Singleton();
	}

	public LinkedList<String> getStuffList()
	{

		return INSTANCE.stuffList;
	}

}

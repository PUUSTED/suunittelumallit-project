package sinkku;

public class ShouldWorkSingleton
{
	protected ShouldWorkSingleton()
	{
	}

	private static class SingletonHolder
	{
		private static final ShouldWorkSingleton INSTANCE = new ShouldWorkSingletonClass();
	}

	public static ShouldWorkSingleton getInstance()
	{
		return SingletonHolder.INSTANCE;
	}

	public void execute()
	{
		return;
	}
}

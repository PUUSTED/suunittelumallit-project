package sinkku;

public class ShuffleTestStuff
{
	public static void main(final String[] args)
	{
		Runnable getStuff = new GetStuff();

		Runnable getStuffAgain = new GetStuff();

		new Thread(getStuff).start();
		new Thread(getStuffAgain).start();
	}
}

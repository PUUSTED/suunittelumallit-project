package factorymethod;

public class Crash extends AterioivaOtus
{
	@Override
	public Ruoka createRuoka()
	{
		return new Omppu();
	};

	@Override
	public Juoma createJuoma()
	{
		return new Omppu();
	};
}

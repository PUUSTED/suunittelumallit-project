package factorymethod;

public abstract class AterioivaOtus
{
	Ruoka ruoka = null;

	Juoma juoma = null;

	public abstract Ruoka createRuoka();

	public abstract Juoma createJuoma();

	public void aterioi()
	{
		syö();
		juo();
	}

	public void syö()
	{
		if (ruoka == null)
			ruoka = createRuoka();
		System.out.println("Minä syön " + ruoka + ", " + ruoka + " maistuu kanalle?");
	}

	public void juo()
	{
		if (juoma == null)
			juoma = createJuoma();
		System.out.println("Aterian jälkeen " + juoma + " tekee terää!");
	}
}

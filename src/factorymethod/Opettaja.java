package factorymethod;

public class Opettaja extends AterioivaOtus
{

	@Override
	public Ruoka createRuoka()
	{
		return new MunavoiPiirras();
	};

	@Override
	public Juoma createJuoma()
	{
		return new Vesi();
	};

}

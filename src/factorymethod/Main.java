package factorymethod;

public class Main
{

	public static void main(final String[] args)
	{
		AterioivaOtus opettaja = new Opettaja();
		opettaja.aterioi();

		AterioivaOtus opiskelia = new Opiskelia();
		opiskelia.aterioi();

		AterioivaOtus crash = new Crash();
		crash.aterioi();
	}
}

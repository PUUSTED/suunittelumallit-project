package factorymethod;

public class Opiskelia extends AterioivaOtus
{
	@Override
	public Ruoka createRuoka()
	{
		return new Pizza();
	};

	@Override
	public Juoma createJuoma()
	{
		return new Pontikka();
	};

}

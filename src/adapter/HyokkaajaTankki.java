package adapter;

import java.util.Random;

public class HyokkaajaTankki implements Hyokkaaja
{

	Random gene = new Random();

	@Override
	public void ammu()
	{

		int aD = gene.nextInt(10) + 1;

		System.out.println("Hyökkääjä tankki tekee " + aD + " vahinkoa");

	}

	@Override
	public void aja()
	{

		int liikunta = gene.nextInt(5) + 1;

		System.out.println("Hyökkääjä liikkuu " + liikunta + " askeleita");

	}

	@Override
	public void valKuski(final String kuskNimi)
	{

		System.out.println(kuskNimi + " revittelee tankkia");

	}

}
package adapter;

public interface Hyokkaaja
{

	public void ammu();

	public void aja();

	public void valKuski(String kuskNimi);

}
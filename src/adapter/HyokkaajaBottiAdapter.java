package adapter;

public class HyokkaajaBottiAdapter implements Hyokkaaja
{

	HyokkaajaBotti botti;

	public HyokkaajaBottiAdapter(final HyokkaajaBotti newBotti)
	{

		botti = newBotti;

	}

	@Override
	public void ammu()
	{

		botti.lyoNyrkein();

	}

	@Override
	public void aja()
	{

		botti.askelEteen();

	}

	@Override
	public void valKuski(final String kuskNimi)
	{

		botti.ihmisLihaPussi(kuskNimi);

	}

}
package adapter;

import java.util.Random;

public class HyokkaajaBotti
{

	Random gene = new Random();

	public void lyoNyrkein()
	{

		int aD = gene.nextInt(10) + 1;

		System.out.println("Hyökkääjä Botti " + aD + " vahinkoa nyrkein");

	}

	public void askelEteen()
	{

		int liikunta = gene.nextInt(5) + 1;

		System.out.println("Hyökkääjä Botti " + liikunta + " askelta");

	}

	public void ihmisLihaPussi(final String kuskNimi)
	{

		System.out.println("Hyppii " + kuskNimi + " päällä");

	}

}
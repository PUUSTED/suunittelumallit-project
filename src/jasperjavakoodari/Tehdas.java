package jasperjavakoodari;

public interface Tehdas
{
	public abstract Farmari createFarmari();

	public abstract TPaita createTPaita();

	public abstract Lippis createLippis();

	public abstract Kengät createKengät();
}
